#!/usr/bin/env python
'''
ベース画像に端子画像をペーストしてQFNのtype04の天面画像を作成する。
type04...中央にメッキパターンを持ち、二辺に端子を持つ
入力: src_images 配下の画像
出力: /mnt/storage/data/{ユーザー名}/cp2/artificial_cp_images/ 配下
'''
import argparse
import cv2
import os
import pytz
import pathlib
import subprocess

from datetime import datetime
from itertools import product
from tqdm import tqdm

OUTPUT_DIR = "output"
WORK_TYPE = "QFN_type04"

# 現在時刻を取得する。
tokyo_tz = pytz.timezone('Asia/Tokyo')
current_time = datetime.now(tokyo_tz)


def resize(img, new_height_mm, new_width_mm, pixel_per_mm):
    # Convert the size in mm to size in pixels
    new_height_pixel = int(round(new_height_mm * pixel_per_mm))
    new_width_pixel = int(round(new_width_mm * pixel_per_mm))

    if new_height_pixel % 2 != 0:
        new_height_pixel = new_height_pixel + 1
    if new_width_pixel % 2 != 0:
        new_width_pixel = new_width_pixel + 1

    # Resize the image and mask
    return cv2.resize(img, (new_width_pixel, new_height_pixel), interpolation = cv2.INTER_LINEAR)


def create_image_like_QFN(base, part_image_root_dir_path):
    part = cv2.imread(os.path.join(part_image_root_dir_path, "gold.png"))

    # base のサイズを取得
    base_width = base.shape[1]

    # x軸方向に貼り付ける part のサイズを定義する（ダミー）。
    part_size_x = [4, 8]

    # part を張り付けていない部分を 1 pixel のバッファを付けて抜き出す。
    base_small = base[part_size_x[1]+1:-part_size_x[1]-1,:]
    # base_small のサイズを取得
    base_small_height, base_small_width = base_small.shape[:2]
    # y軸方向に貼り付ける part のサイズを定義する。
    part_size_y = [8, 4]
    # partを縮小する。
    part_y = cv2.resize(part, part_size_y)
    # part を貼り付けるy方向の開始位置を計算する。
    base_small_height_half = base_small_height/2
    start_y = int((base_small_height_half - part_size_y[1]/2) % part_size_y[1])
    # 貼り付け可能な part の個数を計算する。
    insertable_part_count = int((base_small_height_half - part_size_y[1]/2) // part_size_y[1])
    # 貼り付け可能な part の個数が奇数の場合は貼り付け開始位置を part 一個分y方向にずらす。
    if insertable_part_count % 2 != 0:
        start_y = start_y + part_size_y[1]
    # part をy方向に貼り付ける。
    for j in range(start_y, base_small_height - start_y, part_size_y[1] * 2):
        base_small[j:j+part_size_y[1], 0:part_size_y[0]] = part_y
        base_small[j:j+part_size_y[1], -part_size_y[0]:base_small_width] = part_y

    # 左端y方向中央に貼り付ける part のサイズを定義する。
    part_size_y_center = [part_size_y[0] * 2, 4]
    # 中央に貼り付ける part のサイズを base の横幅に合わせて調整する。
    if base_width < part_size_y_center[0] * 2:
        part_size_y_center[0] = int(base_width/2)
    part_y_center = cv2.resize(part, part_size_y_center)
    # part を貼り付けるy方向の位置を計算する。
    base_small_height_half = base_small_height/2
    start_y = int(base_small_height_half - part_size_y_center[1]/2)
    base_small[start_y:start_y+part_size_y_center[1], 0:part_size_y_center[0]] = part_y_center

    # 中央に part を貼りつける。
    buff = part_size_y_center[0] - part_size_y[0]
    center_area_top = buff * 2
    center_area_left = part_size_y_center[0] + buff
    center_area_bottom = base_small_height - center_area_top
    center_area_right = base_small_width - part_size_y[0] - center_area_top
    if center_area_bottom > center_area_top and center_area_right > center_area_left:
        part_center = cv2.resize(part, [center_area_right - center_area_left, center_area_bottom - center_area_top])
        base_small[center_area_top:center_area_bottom, center_area_left:center_area_right] = part_center

    return base


def parse_args():
    parser = argparse.ArgumentParser(
        description='Create the top surface of QFN type04 artificially.')
    parser.add_argument('base_image_root_dir_path', help='Root dir of base images')
    parser.add_argument('part_image_root_dir_path', help='Root dir of part images')
    parser.add_argument('--side-resize-min-mm', type=int, default=3, help='Minimum value to resize one side')
    parser.add_argument('--side-resize-max-mm', type=int, default=24, help='Maximum value to resize one side')
    parser.add_argument('--pixel-per-mm', type=float, default=1/0.144, help='pixel per mm when shooting from 200mm')

    return parser.parse_args()


def main():
    args = parse_args()

    resize_range = range(args.side_resize_min_mm, args.side_resize_max_mm + 1)
    # リサイズの全順列を生成する。
    resize_permutaion_list = list(product(resize_range, repeat=2))
    # ベース画像を読み込む。
    base_image = cv2.imread(os.path.join(args.base_image_root_dir_path, "black.png"))
    # 出力ディレクトリを作成する。既存の出力ディレクトリは末尾に現在日付時刻を入れてリネームし、上書きを避ける。
    out_dir = os.path.join(OUTPUT_DIR, WORK_TYPE)
    if os.path.isdir(out_dir):
        os.rename(out_dir, out_dir + "_" + current_time.strftime('%Y%m%d%H%M%S'))
    os.makedirs(out_dir)

    # 進行バーを表示するための設定
    total_loop_num = len(resize_permutaion_list)
    bar = tqdm(total=total_loop_num)
    bar.set_description(WORK_TYPE)
    # リサイズの順列484種類を網羅するように画像を生成する。
    for permutaion in resize_permutaion_list:
        bar.update(1)
        file_name = f'{WORK_TYPE}_{"{:02d}".format(permutaion[0])}x{"{:02d}".format(permutaion[1])}.png'

        # ベースをリサイズする。
        image = resize(
            img=base_image,
            new_height_mm=permutaion[0],
            new_width_mm=permutaion[1],
            pixel_per_mm=args.pixel_per_mm
        )

        # ベースにテクスチャを張り付ける。
        image = create_image_like_QFN(image, args.part_image_root_dir_path)

        cv2.imwrite(os.path.join(out_dir, file_name), image)


def create_output_dir_link():

    # 'whoami' コマンドでユーザー名を取得する。
    result = subprocess.run(['whoami'], stdout=subprocess.PIPE)
    username = result.stdout.decode().strip()

    output_dir_path = f"/mnt/storage/data/{username}/cp2/artificial_cp_images"

    # ディレクトリの存在を確認し、存在しない場合は作成
    if not os.path.exists(output_dir_path):
        os.makedirs(output_dir_path)

    # シンボリックリンクの存在を確認し、存在しない場合は作成
    if not os.path.exists(OUTPUT_DIR):
        pathlib.Path(OUTPUT_DIR).symlink_to(output_dir_path)


if __name__ == '__main__':
    create_output_dir_link()
    main()

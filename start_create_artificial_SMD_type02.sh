#!/bin/bash

# ベース（貼り付け先）となる画像のディレクトリパス（相対パス）を指定する。
base_image_root_dir_path="src_images/base_images"

# 貼り付け元画像のディレクトリパス（相対パス）を指定する。
part_image_root_dir_path="src_images/part_images"

cmd="python create_artificial_SMD_type02.py \
 ${base_image_root_dir_path} \
 ${part_image_root_dir_path} \
"

echo ${cmd}
eval ${cmd}

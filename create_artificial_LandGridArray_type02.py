#!/usr/bin/env python
'''
ベース画像に端子画像をペーストしてLandGridArrayのtype02の天面画像を作成する。
type02...天面中央に端子無し
入力: src_images 配下の画像
出力: /mnt/storage/data/{ユーザー名}/cp2/artificial_cp_images/ 配下
'''
import argparse
import cv2
import numpy as np
import os
import pytz
import pathlib
import subprocess

from copy import copy
from datetime import datetime
from itertools import combinations_with_replacement
from tqdm import tqdm

OUTPUT_DIR = "output"
WORK_TYPE = "LandGridArray_type02"

# 現在時刻を取得する。
tokyo_tz = pytz.timezone('Asia/Tokyo')
current_time = datetime.now(tokyo_tz)


def resize(img, new_height_mm, new_width_mm, pixel_per_mm):
    # Convert the size in mm to size in pixels
    new_height_pixel = int(round(new_height_mm * pixel_per_mm))
    new_width_pixel = int(round(new_width_mm * pixel_per_mm))

    if new_height_pixel % 2 != 0:
        new_height_pixel = new_height_pixel + 1
    if new_width_pixel % 2 != 0:
        new_width_pixel = new_width_pixel + 1

    # Resize the image and mask
    return cv2.resize(img, (new_width_pixel, new_height_pixel), interpolation = cv2.INTER_LINEAR)


def create_image_like_LandGridArray(base, part_image_root_dir_path):
    base_origin = copy(base)
    part = cv2.imread(os.path.join(part_image_root_dir_path, "gray.png"))

    part_length = 4
    pad_pixel = 4
    # partを縮小する。
    part_small = cv2.resize(part, (part_length, part_length))

    mask = np.ones((part_length, part_length), dtype=np.uint8)
    mask[0,0] = 0
    mask[0,-1] = 0
    mask[-1,0] = 0
    mask[-1,-1] = 0
    part_small = cv2.bitwise_and(part_small,part_small,mask=mask)
    part_small[0,0,:] = base[0,0,:]
    part_small[0,-1,:] = base[0,0,:]
    part_small[-1,0,:] = base[0,0,:]
    part_small[-1,-1,:] = base[0,0,:]

    # partを貼り付けるためのエリア（baseのpad_pixel分だけ内側）を取得する。
    base_small = base[pad_pixel:-pad_pixel,pad_pixel:-pad_pixel]
    base_small_height, base_small_width = base_small.shape[:2]

    # partを貼り付ける開始位置を計算する。
    base_small_height_half = base_small_height/2
    base_small_width_half = base_small_width/2
    # x方向の開始位置
    if (base_small_width_half - part_length / 2)//part_length % 2 == 0:
        start_x = int((base_small_width_half - part_length / 2) % part_length) + part_length
    else:
        start_x = int((base_small_width_half - part_length / 2) % part_length)
    # y方向の開始位置
    if (base_small_height_half - part_length / 2)//part_length % 2 == 0:
        start_y = int((base_small_height_half - part_length / 2) % part_length) + part_length
    else:
        start_y = int((base_small_height_half - part_length / 2) % part_length)

    # partを貼り付ける。
    cnt_y = 0
    for i in range(start_y, base_small_height - start_y, part_length * 2):
        cnt_y += 1
        cnt_x = 0
        for j in range(start_x, base_small_width - start_x, part_length * 2):
            cnt_x += 1
            base_small[i:i+part_length, j:j+part_length] = part_small

    # ３×３以上のpartを貼り付けた場合に中央にpartの無いエリアを作る。
    if cnt_y > 2 and cnt_x > 2:
        # partの行数に応じて中央エリアの高さを決定する。
        if cnt_y in [4, 6]:
            hole_size_y = 12
        elif cnt_y == 8:
            hole_size_y = 28
        elif cnt_y == 10:
            hole_size_y = 44
        elif cnt_y == 12:
            hole_size_y = 60
        elif cnt_y == 14:
            hole_size_y = 76
        elif cnt_y == 16:
            hole_size_y = 92
        elif cnt_y == 18:
            hole_size_y = 108
        elif cnt_y == 20:
            hole_size_y = 124
        else:
            hole_size_y = 2

        # partの行数に応じて中央エリアの幅を決定する。
        if cnt_x in [4, 6]:
            hole_size_x = 12
        elif cnt_x == 8:
            hole_size_x = 28
        elif cnt_x == 10:
            hole_size_x = 44
        elif cnt_x == 12:
            hole_size_x = 60
        elif cnt_x == 14:
            hole_size_x = 76
        elif cnt_x == 16:
            hole_size_x = 92
        elif cnt_x == 18:
            hole_size_x = 108
        elif cnt_x == 20:
            hole_size_x = 124
        else:
            hole_size_x = 2

        # 中央エリアを作成してpartを覆い隠す。
        hole = base_origin[:hole_size_y, :hole_size_x]
        hole_top = (base_small.shape[0] - hole_size_y) // 2
        hole_left = (base_small.shape[1] - hole_size_x) // 2
        base_small[hole_top:hole_top+hole_size_y, hole_left:hole_left+hole_size_x] = hole

    # partの貼付や削除で編集したbase_smallをbaseに貼り付ける。
    base[pad_pixel:-pad_pixel,pad_pixel:-pad_pixel] = base_small

    return base


def parse_args():
    parser = argparse.ArgumentParser(
        description='Create the top surface of LandGridArray artificially.')
    parser.add_argument('base_image_root_dir_path', help='Root dir of base images')
    parser.add_argument('part_image_root_dir_path', help='Root dir of part images')
    parser.add_argument('--side-resize-min-mm', type=int, default=3, help='Minimum value to resize one side')
    parser.add_argument('--side-resize-max-mm', type=int, default=24, help='Maximum value to resize one side')
    parser.add_argument('--pixel-per-mm', type=float, default=1/0.144, help='pixel per mm when shooting from 200mm')

    return parser.parse_args()


def main():
    args = parse_args()

    resize_range = range(args.side_resize_min_mm, args.side_resize_max_mm + 1)
    # リサイズの全組み合わせを生成する。
    resize_combination_list = list(combinations_with_replacement(resize_range, 2))
    # ベース画像を読み込む。
    base_image = cv2.imread(os.path.join(args.base_image_root_dir_path, "black.png"))
    # 出力ディレクトリを作成する。既存の出力ディレクトリは末尾に現在日付時刻を入れてリネームし、上書きを避ける。
    out_dir = os.path.join(OUTPUT_DIR, WORK_TYPE)
    if os.path.isdir(out_dir):
        os.rename(out_dir, out_dir + "_" + current_time.strftime('%Y%m%d%H%M%S'))
    os.makedirs(out_dir)

    # 進行バーを表示するための設定
    total_loop_num = len(resize_combination_list)
    bar = tqdm(total=total_loop_num)
    bar.set_description(WORK_TYPE)

    # リサイズの全組み合わせ253種類を網羅するように画像を生成する。
    for combination in resize_combination_list:
        bar.update(1)
        file_name = f'{WORK_TYPE}_{"{:02d}".format(combination[0])}x{"{:02d}".format(combination[1])}.png'

        # ベースをリサイズする。
        image = resize(
            img=base_image,
            new_height_mm=combination[0],
            new_width_mm=combination[1],
            pixel_per_mm=args.pixel_per_mm
        )

        # ベースにテクスチャを張り付ける。
        image = create_image_like_LandGridArray(image, args.part_image_root_dir_path)

        cv2.imwrite(os.path.join(out_dir, file_name), image)


def create_output_dir_link():

    # 'whoami' コマンドでユーザー名を取得する。
    result = subprocess.run(['whoami'], stdout=subprocess.PIPE)
    username = result.stdout.decode().strip()

    output_dir_path = f"/mnt/storage/data/{username}/cp2/artificial_cp_images"

    # ディレクトリの存在を確認し、存在しない場合は作成
    if not os.path.exists(output_dir_path):
        os.makedirs(output_dir_path)

    # シンボリックリンクの存在を確認し、存在しない場合は作成
    if not os.path.exists(OUTPUT_DIR):
        pathlib.Path(OUTPUT_DIR).symlink_to(output_dir_path)


if __name__ == '__main__':
    create_output_dir_link()
    main()

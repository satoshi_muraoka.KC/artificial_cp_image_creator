#!/bin/bash

# 作成するワークタイプを指定する。
declare -a python_scripts=( \
 "create_artificial_LandGridArray.py" \
 "create_artificial_QFN_type01.py" \
 "create_artificial_QFN_type02.py" \
 "create_artificial_QFN_type03.py" \
 "create_artificial_QFN_type04.py" \
 "create_artificial_SMD_type01.py" \
 "create_artificial_SMD_type02.py" \
 "create_artificial_SMD_type03.py" \
 "create_artificial_SMD_type04.py" \
)

# ベース（貼り付け先）となる画像のディレクトリパス（相対パス）を指定する。
base_image_root_dir_path="src_images/base_images"

# 貼り付け元画像のディレクトリパス（相対パス）を指定する。
part_image_root_dir_path="src_images/part_images"

# Loop over the array
for script in "${python_scripts[@]}"
do
  cmd="python ${script} \
    ${base_image_root_dir_path} \
    ${part_image_root_dir_path} \
  "
  echo ${cmd}
  eval ${cmd}
done
#!/usr/bin/env python
'''
ベース画像に端子画像をランダムにペーストしてセラミックパッケージの天面画像を作成する。
入力: src_images 配下の画像
出力: /mnt/storage/data/{ユーザー名}/cp2/artificial_cp_images/ 配下
'''
import argparse
import cv2
import os
import numpy as np
import pytz
import pathlib
import random
import subprocess

from datetime import datetime
from itertools import combinations_with_replacement
from tqdm import tqdm

random.seed(0)
np.random.seed(0)

OUTPUT_DIR = "output"
WORK_TYPE = "random_texture"

# 現在時刻を取得する。
tokyo_tz = pytz.timezone('Asia/Tokyo')
current_time = datetime.now(tokyo_tz)


def resize(img, new_height_mm, new_width_mm, pixel_per_mm):
    # Convert the size in mm to size in pixels
    new_height_pixel = int(round(new_height_mm * pixel_per_mm))
    new_width_pixel = int(round(new_width_mm * pixel_per_mm))

    if new_height_pixel % 2 != 0:
        new_height_pixel = new_height_pixel + 1
    if new_width_pixel % 2 != 0:
        new_width_pixel = new_width_pixel + 1

    # Resize the image and mask
    return cv2.resize(img, (new_width_pixel, new_height_pixel), interpolation = cv2.INTER_LINEAR)


def create_image_like_CP_by_random_texture(base_black, part_image_root_dir_path):
    part_gold = cv2.imread(os.path.join(part_image_root_dir_path, "gold.png"))
    part_gray = cv2.imread(os.path.join(part_image_root_dir_path, "gray.png"))

    base_black_mask = np.zeros_like(base_black[:,:,0])

    base_black_h, base_black_w, _ = base_black.shape

    # base の短い方を基準に part の最大サイズを決める。
    part_size_max = base_black_h if base_black_h < base_black_w else base_black_w
    part_size_max = int(part_size_max / 5) if int(part_size_max / 5) % 2 == 0 else int(part_size_max / 5) + 1
    assert part_size_max > 2
    resize_pixel_list = [i for i in range(2, part_size_max + 1, 2)]

    while True:
        # ランダムサイズ取得
        size_y = np.random.choice(resize_pixel_list)
        size_x = np.random.choice(resize_pixel_list)

        # ランダムpart取得
        part_type = np.random.choice(["gold", "gray"])

        if part_type=="gold":
            resize_part = cv2.resize(part_gold, (size_x, size_y))
        elif part_type=="gray":
            resize_part = cv2.resize(part_gray, (size_x, size_y))

        resize_part_h, resize_part_w, _ = resize_part.shape

        # ランダム貼付座標top_left取得
        paste_top = np.random.randint(0, base_black_h)
        paste_left = np.random.randint(0, base_black_w)

        # 添付座標bottom_right計算
        paste_bottom = paste_top + resize_part_h
        paste_right = paste_left + resize_part_w

        # 画像からはみ出す場合の調整
        paste_bottom = base_black_h if paste_bottom > base_black_h else paste_bottom
        paste_right = base_black_w if paste_right > base_black_w else paste_right

        # 調整後の貼付領域のサイズにpartイメージを合わせる
        if paste_bottom - paste_top != resize_part_h \
            or paste_right - paste_left != resize_part_w:
            new_part_h = paste_bottom - paste_top
            new_part_w = paste_right - paste_left
            resize_part = cv2.resize(resize_part, (new_part_w, new_part_h))

        # print(resize_part.shape)
        # print(paste_bottom - paste_top, paste_right - paste_left)

        # 貼り付け
        base_black[paste_top:paste_bottom, paste_left:paste_right] = resize_part

        # マスクの更新
        base_black_mask[paste_top:paste_bottom, paste_left:paste_right] = np.ones_like(resize_part[:,:,0])

        #
        mask_ratio = np.count_nonzero(base_black_mask) / base_black_mask.size
        if mask_ratio > 0.5:
            break

    return base_black


def parse_args():
    parser = argparse.ArgumentParser(
        description='Create the top surface of CP artificially.')
    parser.add_argument('base_image_root_dir_path', help='Root dir of base images')
    parser.add_argument('part_image_root_dir_path', help='Root dir of part images')
    parser.add_argument('--side-resize-min-mm', type=int, default=3, help='Minimum value to resize one side')
    parser.add_argument('--side-resize-max-mm', type=int, default=24, help='Maximum value to resize one side')
    parser.add_argument('--pixel-per-mm', type=float, default=1/0.144, help='pixel per mm when shooting from 200mm')

    return parser.parse_args()


def main():
    args = parse_args()

    resize_range = range(args.side_resize_min_mm, args.side_resize_max_mm + 1)
    # リサイズの全組み合わせを生成する。
    resize_combination_list = list(combinations_with_replacement(resize_range, 2))
    # ベース画像を読み込む。
    base_image = cv2.imread(os.path.join(args.base_image_root_dir_path, "black.png"))
    # 出力ディレクトリを作成する。既存の出力ディレクトリは末尾に現在日付時刻を入れてリネームし、上書きを避ける。
    out_dir = os.path.join(OUTPUT_DIR, WORK_TYPE)
    if os.path.isdir(out_dir):
        os.rename(out_dir, out_dir + "_" + current_time.strftime('%Y%m%d%H%M%S'))
    os.makedirs(out_dir)

    # 進行バーを表示するための設定
    total_loop_num = len(resize_combination_list)
    bar = tqdm(total=total_loop_num)
    bar.set_description(WORK_TYPE)

    # リサイズの全組み合わせ253種類を網羅するように画像を生成する。
    for combination in resize_combination_list:
        bar.update(1)
        file_name = f'{WORK_TYPE}_{"{:02d}".format(combination[0])}x{"{:02d}".format(combination[1])}.png'

        # ベースをリサイズする。
        image = resize(
            img=base_image,
            new_height_mm=combination[0],
            new_width_mm=combination[1],
            pixel_per_mm=args.pixel_per_mm
        )

        # ベースにテクスチャを張り付ける。
        image = create_image_like_CP_by_random_texture(image, args.part_image_root_dir_path)

        cv2.imwrite(os.path.join(out_dir, file_name), image)


def create_output_dir_link():

    # 'whoami' コマンドでユーザー名を取得する。
    result = subprocess.run(['whoami'], stdout=subprocess.PIPE)
    username = result.stdout.decode().strip()

    output_dir_path = f"/mnt/storage/data/{username}/cp2/artificial_cp_images"

    # ディレクトリの存在を確認し、存在しない場合は作成
    if not os.path.exists(output_dir_path):
        os.makedirs(output_dir_path)

    # シンボリックリンクの存在を確認し、存在しない場合は作成
    if not os.path.exists(OUTPUT_DIR):
        pathlib.Path(OUTPUT_DIR).symlink_to(output_dir_path)


if __name__ == '__main__':
    create_output_dir_link()
    main()

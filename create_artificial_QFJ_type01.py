#!/usr/bin/env python
'''
ベース画像に端子画像をペーストしてQFJのtype01の天面画像を作成する。
type01...中央にメッキパターンを持ち、四辺に端子を持つ
入力: src_images 配下の画像
出力: /mnt/storage/data/{ユーザー名}/cp2/artificial_cp_images/ 配下
'''
import argparse
import cv2
import numpy as np
import os
import pytz
import pathlib
import subprocess

from datetime import datetime
from itertools import combinations_with_replacement
from tqdm import tqdm

OUTPUT_DIR = "output"
WORK_TYPE = "QFJ_type01"

# 現在時刻を取得する。
tokyo_tz = pytz.timezone('Asia/Tokyo')
current_time = datetime.now(tokyo_tz)


protruding_terminal_size = 4

def resize(img, new_height_mm, new_width_mm, pixel_per_mm):
    # Convert the size in mm to size in pixels
    new_height_pixel = int(round(new_height_mm * pixel_per_mm))
    new_width_pixel = int(round(new_width_mm * pixel_per_mm))

    if new_height_pixel % 2 != 0:
        new_height_pixel = new_height_pixel + 1
    if new_width_pixel % 2 != 0:
        new_width_pixel = new_width_pixel + 1

    # Resize the image and mask
    return cv2.resize(img, (new_width_pixel, new_height_pixel), interpolation = cv2.INTER_LINEAR)


def create_image_like_QFJ(base, part_image_root_dir_path):
    part = cv2.imread(os.path.join(part_image_root_dir_path, "gold.png"))

    # base のサイズを取得
    base_height, base_width = base.shape[:2]

    # x軸方向に貼り付ける part のサイズを定義する。
    part_size_x = [4, 8 + protruding_terminal_size]
    # part を縮小する。
    part_x = cv2.resize(part, part_size_x)
    # part を貼り付けるx方向の開始位置を計算する。
    base_width_half = base_width/2
    start_x = int((base_width_half - part_size_x[0] / 2) % part_size_x[0])
    # part をx方向に貼り付ける。
    for i in range(start_x, base_width - start_x, part_size_x[0] * 2):
        if i == start_x or i > base_width - part_size_x[0] * 2:
            continue
        base[0:part_size_x[1], i:i+part_size_x[0]] = part_x
        base[-part_size_x[1]:base_height, i:i+part_size_x[0]] = part_x
        # cv2.imwrite("./conf.png", base)
        # input(i)

    # part を張り付けていない部分を 1 pixel のバッファを付けて抜き出す。
    base_small = base[part_size_x[1]+1:-part_size_x[1]-1,:]
    # base_small のサイズを取得
    base_small_height, base_small_width = base_small.shape[:2]
    # y軸方向に貼り付ける part のサイズを定義する。
    part_size_y = [8 + protruding_terminal_size, 4]
    # partを縮小する。
    part_y = cv2.resize(part, part_size_y)
    # part を貼り付けるx方向の開始位置を計算する。
    base_small_height_half = base_small_height/2
    start_y = int((base_small_height_half - part_size_y[1] / 2) % part_size_y[1])
    # part をy方向に貼り付ける。
    for j in range(start_y, base_small_height - start_y, part_size_y[1] * 2):
        base_small[j:j+part_size_y[1], 0:part_size_y[0]] = part_y
        base_small[j:j+part_size_y[1], -part_size_y[0]:base_small_width] = part_y

    # 中央に part を貼りつける。
    buff = part_size_y[0]
    center_area_top = buff
    center_area_left = part_size_y[0] + buff
    center_area_bottom = base_small_height - center_area_top
    center_area_right = base_small_width - part_size_y[0] - center_area_top
    if center_area_bottom > center_area_top and center_area_right > center_area_left:
        part_center = cv2.resize(part, [center_area_right - center_area_left, center_area_bottom - center_area_top])
        base_small[center_area_top:center_area_bottom, center_area_left:center_area_right] = part_center

    base[part_size_x[1]+1:-part_size_x[1]-1,:] = base_small

    return base


def parse_args():
    parser = argparse.ArgumentParser(
        description='Create the top surface of QFJ type01 artificially.')
    parser.add_argument('base_image_root_dir_path', help='Root dir of base images')
    parser.add_argument('part_image_root_dir_path', help='Root dir of part images')
    parser.add_argument('--side-resize-min-mm', type=int, default=3, help='Minimum value to resize one side')
    parser.add_argument('--side-resize-max-mm', type=int, default=24, help='Maximum value to resize one side')
    parser.add_argument('--pixel-per-mm', type=float, default=1/0.144, help='pixel per mm when shooting from 200mm')

    return parser.parse_args()


def main():
    args = parse_args()

    resize_range = range(args.side_resize_min_mm, args.side_resize_max_mm + 1)
    # リサイズの全組み合わせを生成する。
    resize_combination_list = list(combinations_with_replacement(resize_range, 2))
    # ベース画像を読み込む。
    base_image = cv2.imread(os.path.join(args.base_image_root_dir_path, "black.png"))
    # 出力ディレクトリを作成する。既存の出力ディレクトリは末尾に現在日付時刻を入れてリネームし、上書きを避ける。
    out_dir = os.path.join(OUTPUT_DIR, WORK_TYPE)
    if os.path.isdir(out_dir):
        os.rename(out_dir, out_dir + "_" + current_time.strftime('%Y%m%d%H%M%S'))
    os.makedirs(out_dir)

    # 進行バーを表示するための設定
    total_loop_num = len(resize_combination_list)
    bar = tqdm(total=total_loop_num)
    bar.set_description(WORK_TYPE)

    # リサイズの全組み合わせ253種類を網羅するように画像を生成する。
    for combination in resize_combination_list:
        bar.update(1)
        file_name = f'{WORK_TYPE}_{"{:02d}".format(combination[0])}x{"{:02d}".format(combination[1])}.png'

        # ベースをリサイズする。
        image = resize(
            img=base_image,
            new_height_mm=combination[0],
            new_width_mm=combination[1],
            pixel_per_mm=args.pixel_per_mm
        )

        # 画像をパディングする。
        image = cv2.resize(image, (image.shape[1] + protruding_terminal_size * 2, image.shape[0] + protruding_terminal_size * 2), interpolation = cv2.INTER_LINEAR)

        # ベースにテクスチャを張り付ける。
        image = create_image_like_QFJ(image, args.part_image_root_dir_path)

        # アルファチャンネルを追加する。
        alpha_channel = np.ones(image.shape[:2], dtype=image.dtype) * 255
        image_rgba = cv2.merge((image, alpha_channel))

        # 画像外周かつベース画像と同色の場合、アルファチャンネルを0に変更する。
        outer_circumference_mask = np.ones(image_rgba.shape[:2], dtype=np.uint8)
        outer_circumference_mask[protruding_terminal_size:-protruding_terminal_size, protruding_terminal_size:-protruding_terminal_size] = 0
        # マスク内の箇所で色Aと一致するピクセルを探す
        match = (image[:, :, :3] == base_image[0,0,:]).all(axis=2)
        # マスクとmatchの両方に該当する箇所のアルファチャンネルを0にする
        image_rgba[np.logical_and(outer_circumference_mask, match), 3] = 0

        cv2.imwrite(os.path.join(out_dir, file_name), image_rgba)


def create_output_dir_link():

    # 'whoami' コマンドでユーザー名を取得する。
    result = subprocess.run(['whoami'], stdout=subprocess.PIPE)
    username = result.stdout.decode().strip()

    output_dir_path = f"/mnt/storage/data/{username}/cp2/artificial_cp_images"

    # ディレクトリの存在を確認し、存在しない場合は作成
    if not os.path.exists(output_dir_path):
        os.makedirs(output_dir_path)

    # シンボリックリンクの存在を確認し、存在しない場合は作成
    if not os.path.exists(OUTPUT_DIR):
        pathlib.Path(OUTPUT_DIR).symlink_to(output_dir_path)


if __name__ == '__main__':
    create_output_dir_link()
    main()
